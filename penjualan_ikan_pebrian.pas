program penjualan_ikan;

// Nama : Pebrian Agung Gumilang
// NPM : 20231310051
// Prodi : Teknik Informatika A2

uses crt;

type
  TIkan = record
    id: integer;
    namaIkan: string;
    stock: integer;
    harga: integer;
  end;

var
  Ikan: array of TIkan;
  i: integer;
  ikanId: integer;
  bobot: integer;
  total: LongInt;

function totalBayar(total: LongInt): LongInt;
begin
  if total >= 150000 then // Kondisi diskon 15%
    totalBayar := total - Round(total * 0.15)
  else if total >= 100000 then // Kondisi diskon 10%
    totalBayar := total - Round(total * 0.10)
  else
    totalBayar := total;
end;

procedure InputBobot(params: boolean);
begin
  if params then
    writeln('Bobot yang diinput melebihi stock, silahkan input kembali ');

  write('Input bobot ikan : '); readln(bobot);

  if Ikan[ikanId].stock < bobot then
    InputBobot(true)
  else
  begin
    total := Ikan[ikanId].harga * bobot;
    
    writeln('===========================================================');
    writeln('              TOTAL BAYAR = ',totalBayar(total),'                  ');
    writeln('===========================================================');
  end;
end;

begin
    SetLength(Ikan, 10);

    Ikan[1].id := 1;
    Ikan[1].namaIkan := 'Gabus ';
    Ikan[1].stock := 23;
    Ikan[1].harga := 10000;

    Ikan[2].id := 2;
    Ikan[2].namaIkan := 'Arwana';
    Ikan[2].stock := 16;
    Ikan[2].harga := 12000;

    Ikan[3].id := 3;
    Ikan[3].namaIkan := 'Kerapu';
    Ikan[3].stock := 10;
    Ikan[3].harga := 15000;

    Ikan[4].id := 4;
    Ikan[4].namaIkan := 'Kakap ';
    Ikan[4].stock := 20;
    Ikan[4].harga := 20000;

    Ikan[5].id := 5;
    Ikan[5].namaIkan := 'Tuna  ';
    Ikan[5].stock := 30;
    Ikan[5].harga := 10000;

    clrscr;
    writeln('===========================================================');
    writeln('=              PRICE LIST DAN STOCK IKAN                  =');
    writeln('===========================================================');
    writeln('= ID =    NAMA IKAN     =    Harga(Rp)  =    Stock(KG)    =');

    for i := 1 to 5 do
    begin
        writeln('= ', Ikan[i].id, '  =    ', Ikan[i].namaIkan, '        =    ', Ikan[i].harga, '      =    ', Ikan[i].stock, '           =');
    end;

    write('Pilih ikan yang akan anda beli menggunakan ID : '); readln(ikanId);
    InputBobot(False);

    readln;
end.
